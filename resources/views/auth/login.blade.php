@extends('layouts.app')
@section('content')
    <!-- Start wrapper-->
    <div id="wrapper">
        <div class="card card-authentication1 mx-auto my-4">
            <div class="card-body">
                <div class="card-content p-2">
                    <div class="text-center">
                        <img src="assets/images/logo-icon.png" alt="logo icon">
                    </div>
                <div class="card-title text-uppercase text-center py-3">Sign In</div>
                @if(session()->has('error'))
                    <div class="text-danger text-center">
                        {{ session()->get('error') }}
                    </div>
                @endif
                <br>
                <form method="POST" action="{{ route('login') }}" class="md-float-material">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputUsername" class="sr-only">Email</label>
                        <div class="position-relative has-icon-right">
                            <input type="email" id="exampleInputUsername" class="form-control input-shadow @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Enter email">
                            <div class="form-control-position">
                                <i class="icon-user"></i>
                            </div>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>     
        
                    <div class="form-group">
                        <label for="exampleInputPassword" class="sr-only">Password</label>
                        <div class="position-relative has-icon-right">
                            <input type="password" id="exampleInputPassword" class="form-control input-shadow @error('password') is-invalid @enderror" name="password" placeholder="Enter Password">
                            <div class="form-control-position">
                                <i class="icon-lock"></i>
                            </div>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                                           
                    <div class="form-row">
                        <div class="form-group col-6">
                            <div class="icheck-material-white">
                                <input type="checkbox" id="user-checkbox" checked="" />
                                <label for="user-checkbox">Remember me</label>
                            </div>
                        </div>
                        <div class="form-group col-6 text-right">
                            <a href="{{ route('forget-password') }}">Reset Password</a>
                        </div>
                    </div>
        
                    <button type="submit" class="btn btn-light btn-block">Sign In</button>
                    <div class="text-center mt-3">Sign In With</div>
                    <div class="form-row mt-4">
                        <div class="form-group mb-0 col-6">
                            <button type="button" class="btn btn-light btn-block"><i class="fa fa-facebook-square"></i> Facebook</button>
                        </div>
                        <div class="form-group mb-0 col-6 text-right">
                            <button type="button" class="btn btn-light btn-block"><i class="fa fa-twitter-square"></i> Twitter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card-footer text-center py-3">
            <p class="text-warning mb-0">Do not have an account? <a href="{{route('register')}}"> Sign Up here</a></p>
        </div>
    </div>
@endsection
